from django.contrib.auth.models import User, Group
from models import Item, Recipe, Ingredient
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = ('id','name','unit','price')

class RecipeSerialzer(serializers.HyperlinkedModelSerializer):
    recipe_price = serializers.CharField(source='get_recipe_price', read_only=True)
    item_list = serializers.CharField(source='get_recipe_items', read_only=True)
    class Meta:
        model = Recipe
        fields = ('id','name', 'servings', 'recipe_price', 'item_list')

class IngredientSerialzer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('ingredient_recipe_id','recipe_id','item_id','quantity')
