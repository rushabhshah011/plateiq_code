var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://127.0.0.1:8000/recipes/?format=json",
    "method": "GET",
    "headers": {
        "Cache-Control": "no-cache",
        "Postman-Token": "a8b683d7-f3e8-4145-a916-7e5051eaddbb"
    }
}

$.ajax(settings).done(function(response) {
    table = $('#example').DataTable({
        data: response,
        "columns": [{
                data: "id"
            },
            {
                data: "name"
            },
            {
                data: "servings"
            },
            {
                data: "recipe_price"
            },
            {
                data: "item_list"
            },
            {
                data: null,
                className: "center",
                defaultContent: '<a href="#recipe-edit-form" class="editor_edit" rel="modal:open">Edit</a> '
            },
            {
                data: null,
                className: "center",
                defaultContent: '<a href="#add-ingredients-form" class="add-ingredients" rel="modal:open">add ingredients</a> '
            }
        ]
    });


});

$(document).on('click', '.editor_edit', function() {
    var data = window.table.row($(this).parents('tr')).data();
    document.getElementById("recipe_edit_id").value = data.id
    document.getElementById("recipe_edit_name").value = data.name
    document.getElementById("recipe_edit_serving").value = data.servings
    document.getElementById("recipe_edit_Ingredients").innerText = data.item_list
});

$(document).on('click', '.add-ingredients', function() {
    var data = window.table.row($(this).parents('tr')).data();
    document.getElementById("recipe_id").innerText = data.id
    document.getElementById("recipe_Ingredients").innerText = data.name
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://127.0.0.1:8000/items/",
        "method": "GET",
        "headers": {
            "Cache-Control": "no-cache",
            "Postman-Token": "0aafdc90-90e8-40c6-a312-a9898f4296a1"
        }
    }

    $.ajax(settings).done(function(response) {
        var $dropdown = $("#add-ingredients-menu");
        $dropdown.empty();
        $.each(response, function() {
            $dropdown.append($("<option />").val(this.id).text(this.name));
        });
    });

});

function editrecipe() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://127.0.0.1:8000/recipes/" + document.getElementById("recipe_edit_id").value + "/",
        "method": "PUT",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "4be256f7-cf52-48e0-a6be-86658c76cd84"
        },
        "processData": false,
        "data": "{\n    \"name\": \"" + document.getElementById("recipe_edit_name").value + "\",\n    \"servings\": \"" + document.getElementById("recipe_edit_serving").value + "\"\n}"
    }

    $.ajax(settings).done(function(response) {
        console.log(response);
    });

}


function searchfunc() {
    urlstring = "http://127.0.0.1:8000/recipes/?search=" + document.getElementById("searchtext").value
    var search_settings = {
        "async": true,
        "crossDomain": true,
        "url": urlstring,
        "method": "GET",
        "headers": {
            "Cache-Control": "no-cache",
            "Postman-Token": "a8b683d7-f3e8-4145-a916-7e5051eaddbb"
        }
    }

    $.ajax(search_settings).done(function(response) {
        table.destroy();
        $('#example').DataTable({
            data: response,
            "columns": [{
                    data: "id"
                },
                {
                    data: "name"
                },
                {
                    data: "servings"
                },
                {
                    data: "recipe_price"
                },
                {
                    data: null,
                    className: "center",
                    defaultContent: '<a href="#recipe-edit-form" id="btnEdit" class="editor_edit">Edit</a>'
                }
            ]
        });

    });

}

function postnewrecipe() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://127.0.0.1:8000/recipes/",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "e968c2d8-2ee9-4bb8-9b47-a80084e5f4b2"
        },
        "processData": false,
        "data": "{\r\n    \"name\": \"" + document.getElementById("recipe_name").value + "\",\r\n    \"servings\": \"" + document.getElementById("recipe_serving").value + "\"\r\n}"
    }

    $.ajax(settings).done(function(response) {
        console.log(response);
    });

}

function additem() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://127.0.0.1:8000/items/",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "22117815-b189-43e5-8070-dfe42ee27432"
        },
        "processData": false,
        "data": "{\n    \"name\": \"" + document.getElementById("item_name").value + "\",\n    \"unit\": \"" + document.getElementById("item_unit").value + "\",\n    \"price\": " + document.getElementById("price_unit").value + "\n}"
    }

    $.ajax(settings).done(function(response) {
        console.log(response);
    });
}

function addingredient() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://127.0.0.1:8000/ingredient/",
        "method": "POST",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "b1aac803-1733-414d-bc6e-f85b80e8931f"
        },
        "processData": false,
        "data": "{\n    \"recipe_id\": \"http://127.0.0.1:8000/recipes/" + document.getElementById("recipe_id").innerText + "/\",\n    \"item_id\": \"http://127.0.0.1:8000/items/" + $("#add-ingredients-menu").children(":selected").attr("value") + "/\",\n    \"quantity\": " + document.getElementById("quantity").value + "\n}"
    }

    $.ajax(settings).done(function(response) {
        console.log(response);
    });

}
