# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics
from rest_framework.filters import SearchFilter
from plateiq.plateiq_app.serializers import UserSerializer, GroupSerializer, ItemSerializer, RecipeSerialzer, IngredientSerialzer
from models import Item, Recipe, Ingredient
from django_filters import rest_framework as filters
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter,)
    filterset_fields = ('username', 'email')
    search_fields = ('username', 'email')

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter,)
    filterset_fields = ('id','name')
    search_fields = ('id','name')

class RecipeViewSet(viewsets.ModelViewSet):
    queryset = Recipe.objects.all()
    serializer_class =  RecipeSerialzer
    filter_backends = (filters.DjangoFilterBackend, SearchFilter,)
    filterset_fields = ('id','name')
    search_fields = ('id','name')

class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class =  IngredientSerialzer

from django.shortcuts import render_to_response

def index (request):
    return render_to_response('index.html')


