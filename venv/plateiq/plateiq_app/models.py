# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Item(models.Model):
	name = models.CharField(max_length=100)
	unit = models.CharField(max_length=100, default='0')
	price = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)

	def __str__(self):
		return self.name


class Recipe(models.Model):
	name = models.CharField(max_length=100)
	servings = models.CharField(max_length=100, default='0')

	def get_recipe_price(self):
		item_id_list = Ingredient.objects.filter(recipe_id=self.id).values_list('item_id', flat=True)
		price_list = Item.objects.filter(id__in=item_id_list).values_list('price', flat=True)
		return sum(price_list)

	def get_recipe_items(self):
		item_id_list = Ingredient.objects.filter(recipe_id=self.id).values_list('item_id', flat=True)
		item_name_list = Item.objects.filter(id__in=item_id_list).values_list('name', flat=True)
		return ', '.join(item_name_list)

	def __str__(self):
		return self.name


class Ingredient(models.Model):
	recipe_id = models.ForeignKey('Recipe', null=True)
	item_id = models.ForeignKey('Item', null=True)
	ingredient_recipe_id = models.AutoField(primary_key=True)
	quantity = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
