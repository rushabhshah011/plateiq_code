This is the python project with the virtual environment setup for ease of usage and consistency through out versions.

To Run

For Mac OSX only

brew install python@2
sudo easy_install pip <br>


1. Clone the repo<br/>
2. cd to the repo directory
3. cd venv <br>
4. source bin/activate <br>
5. pip install -r requirements.txt <br>
6. python2 manage.py runserver <br>

Open Following Link in Browser to access App :

http://127.0.0.1:8000/index/

Open Following Link in Browser to access Browsable APIs :

http://127.0.0.1:8000/


Thank you